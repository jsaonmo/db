# Db
>db 使用案例
### SELECT
>最好都使用pdo参数绑定
>>where 用法 

    $where|$sql
    ---|---
    $where = ['user_id'=>1];|user_id = '1';
    $where = ['user_id'=>[1,2,3]]|user_id IN('1','2','3')
    $where = ['user_id >'=>'1']|user_id >'1'
    $where = ['user_id like'=>'%m%']|user_id like '%m%'
    $where = [' OR user_id'=>'1']| OR  user_id = 1  
    $where = [' AND user_id'=>'1']| AND user_id = 1
#### getRow
> 返回一维数组

    $where = ['user_id'=>123];
    $fields = [];
    $order = null;
    $table = null
    $result = $this->getRow($where, $fields, $order, $table);
    
#### getRows 
> 返回二维数组

    $where = ['user_id'=>123];
    $fields = [];
    $order = null;
    $table = null
    $limit = null
    $result = $this->getRows($where, $fields, $order, $limit, $table);
    
    
#### getRowsBySql
> 返回二维数组

    $sql = 'SELECT * FROM USER WHERE user_id=? and name like ? and id in (?,?,?)';
    $bind = ['123','好的','1','2','3'];
    $this->getRowsBySql($sql, $bind);
    
    SELECT * FROM USER WHERE user_id='123' and name like '好的' and id in ('1','2','3')
#### getLastSql
> 返回最近一次sql语句

    $this->getLastSql(); 
#### getDb
> 返回 pdo对象

    $this->getDb();
    
### DELETE
>返回影响行数

    $this->delete($where)
    
### UPDATE
>返回影响行数 

    $this->update($row, $where)
### INSERT
>返回 主键id

    $data = ['user_id'=>1]
    $this->add($data)
### 事物
> 开启
    
    $this->getDb()->startTrans();
    
> 提交

    $this->getDb()->commit();
    
> 回滚

    $this->getDb()->rollback();
### 写日志
> 配置文件 is_log = true

     $this->getDb()->write($str);
## 基础类
    use Jason\Db\Model;
    class BaseModel extends Model
    {
        public function init()
        {
            $config = [
                'type'     => 'mysql',                       //pdo 链接方式
                'username' => 'root,docker',                 //用户名
                'password' => '111111,1234',                 //密码
                'hostname' => '127.0.0.1,172.*.*.248',       //ip
                'hostport' => '3306,3316',                   //端口
                'database' => 'zuju,zuju',                   //数据库
                'charset'  => 'utf8,utf8',                   //字符集
                'deploy'  => true,                           //采用分布式数据库
                'rw_separate'  => true,                      // 主从式采用读写分离
                'is_log'  => true,                           //是否记录日志
            ];
            $this->config = $config;
        }
    }